function string2(IPAddress) {
    if (typeof IPAddress !== 'string') {
        return [];
    }

    const numbers = IPAddress.split('.');
    
    for (let key in numbers) {
        numbers[key] = parseInt(numbers[key]);
        if (isNaN(numbers[key]) === true) {
            return [];
        } 
    }

    return numbers;

}

module.exports = string2;